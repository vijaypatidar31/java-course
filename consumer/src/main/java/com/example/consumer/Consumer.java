package com.example.consumer;

import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Consumer {
    private final ArrayBlockingQueue<Integer> arrayBlockingQueue;

    public Consumer(ArrayBlockingQueue<Integer> arrayBlockingQueue) {
        this.arrayBlockingQueue = arrayBlockingQueue;
    }

    public void startConsumption(){
        Runnable runnable = () -> {
            while (true){
                try {
                    try {
                        Integer remove = arrayBlockingQueue.remove();
                        System.out.println("Consumer : "+remove);
                    }catch (NoSuchElementException e){
//                        e.printStackTrace();
                        System.err.println("Product out of stock 😂 nothing to consume");
                    }
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        new Thread(runnable).start();
    }
}
