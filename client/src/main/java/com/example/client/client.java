package com.example.client;

import com.example.consumer.Consumer;
import com.example.producer.Producer;

import java.util.concurrent.ArrayBlockingQueue;

public class client {
    public static void main(String[] args) {
        ArrayBlockingQueue<Integer> arrayBlockingQueue = new ArrayBlockingQueue<>(10);
        Producer producer = new Producer(arrayBlockingQueue);
        producer.startProduction();

        Consumer consumer = new Consumer(arrayBlockingQueue);
        consumer.startConsumption();
    }
}
