public class DoWhile {
    public static void main(String... arg) {

        int value = 10;

        //this loop run at least one time regardless on value of value var
        do {
            System.out.println(value + " " + (value % 2 == 0 ? " is even" : " is odd"));
            value++;
        } while (value < 100);
    }
}
