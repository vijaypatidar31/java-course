class Machine {
    public String type;

    public Machine() {
        type = "Machine";
    }

    public void start() {
        System.out.println("Machine is Working!");
    }
}

class Bus extends Machine {

    public Bus() {
        type = "Bus";
    }

    public void start() {
        System.out.println("Bus is starting..");
    }


}

class Car extends Machine {

    public Car() {
        type = "Car";
    }

    public void start() {
        System.out.println("Car is Working!");
    }
}


public class Inheritance {

    public static void main(String[] args) {
        Car car = new Car();
        Machine machine = new Car();
        Machine m = new Machine();
        Bus bus = new Bus();

        System.out.println(car.type);
        System.out.println(machine.type);
        System.out.println(m.type);
        System.out.println(bus.type);

        car.start();
        machine.start();
        m.start();
        bus.start();
    }
}