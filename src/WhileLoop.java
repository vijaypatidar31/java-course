
public class WhileLoop {

    public static void main(String[] args) {
        int i =1;
        int n =2;

        while(i<11){
            System.out.println(n+" x "+i+" = "+n*i);
            i++;
        }
    }
}