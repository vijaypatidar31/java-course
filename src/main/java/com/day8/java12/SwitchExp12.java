package com.day8.java12;

import java.util.Scanner;

public class SwitchExp12 {
    public static void main(String[] args) {
        int i =new Scanner(System.in).nextInt();

        String res = switch(i%3){
            case 0 -> "Zero";
            case 1 -> "one";
            default -> "Two";
        };

        System.out.println(res);
    }
}
