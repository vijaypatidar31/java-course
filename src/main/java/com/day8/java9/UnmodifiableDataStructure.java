package com.day8.java9;

import java.util.*;

public class UnmodifiableDataStructure {
    public static void main(String[] args) {
        //before java 9
        List<String> names = new ArrayList<>();
        names.add("Cat");
        names.add("Dog");
        names.add("Elephant");

        List<String> strings = Collections.unmodifiableList(names);
        System.out.println(strings);
        try {
            strings.add("Cow");
        }catch (Exception e){
            System.out.println("Unable to add element");
        }

        Map<String,String> map = new HashMap<>();
        map.put("d","Dog");
        map.put("c","Cow");

        Map<String, String> unmodifiableMap = Collections.unmodifiableMap(map);
        System.out.println(unmodifiableMap);
        try {
            unmodifiableMap.put("e","Elephant");
        }catch (Exception e){
            System.out.println("Unable to modified immutable map");
        }


        //from java 9 , it is easy to create immutable map,list,set...

        List<String> animals = List.of("Cat", "Dog", "Cow");
        System.out.println(animals);

        Set<String> set = Set.of("Cat", "Dog");
//        Set<String> set = Set.of("Cat", "Dog","Cat");//this will throw exception
        System.out.println(set);

        Map<String, String> map1 = Map.of("c", "cat");
        System.out.println(map1);
    }
}
