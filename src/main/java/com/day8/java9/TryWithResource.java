package com.day8.java9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

class Resource implements AutoCloseable{

    @Override
    public void close(){
        System.out.println("Resource closed 😁");
    }
}

public class TryWithResource {
    public static void main(String[] args) {
        Resource resource = new Resource();
        //we can use existing resource in try in java 9
        try (resource){
            System.out.println(resource);
        }

        Reader reader = new StringReader("Hello...");
        BufferedReader br = new BufferedReader(reader);

        try(br) {
            System.out.println(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
