package com.day8.java13;

import java.time.LocalDate;

public class SwitchExp13 {
    public static void main(String[] args) {

        int month = LocalDate.now().getMonthValue();

        String quarter = switch(month){
            case 1,2,3->{
                boolean leapYear = LocalDate.now().isLeapYear();
                yield leapYear?"First Quarter and Leap year":"First Quarter and Leap year";
            }
            case 4,5,6->"Second Quarter";
            case 7,8,9->"Third Quarter";
            default -> "Fourth Quarter";
        };

        System.out.println(quarter);
    }
}
