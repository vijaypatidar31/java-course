package com.day8;

public class TestBlocks {
    public static void main(String[] args) {
        String user = """
                {
                    "name":"vijay",
                    "email":"vijay@abc.com"  
                }
                """;

        String roles = """
                roles:["USER","ADMIN"]
                """;


        System.out.println(user);
        System.out.println(roles);

        System.out.println(user.concat(roles));
    }
}
