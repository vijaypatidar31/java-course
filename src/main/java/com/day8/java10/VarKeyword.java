package com.day8.java10;

import com.day1.User;

import java.util.ArrayList;

public class VarKeyword {
    //var name ="fdjk";not allowed
    public static void main(String[] args) {
        //from java 10 we can use var to create variable with inline initialisation
        var list = new ArrayList<>();

        var ar = new int[10];

        var name = "vijay";

        var user = new User();
        user.display();

        var res = 11/2;
        System.out.println(res);

        var res1 = 11d/2;
        System.out.println(res1);
    }
}
