package com.day8.datetime;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class TimeZone {
    public static void main(String[] args) {
        System.out.println("Available zones");
        ZoneId.getAvailableZoneIds().stream().sorted()
                .forEach(System.out::println);


        System.out.println("------------------------");

        System.out.println("Asia/Kolkata : "+LocalDateTime.ofInstant(Instant.now(),ZoneId.of("Asia/Kolkata")));
        System.out.println("US/Hawaii : "+LocalDateTime.ofInstant(Instant.now(),ZoneId.of("US/Hawaii")));


        System.out.println("==========");

        ZoneId.getAvailableZoneIds()
                .stream()
                .sorted()
                .forEach(s -> {
                    System.out.printf("%-20s : %s%n\n", s, LocalDateTime.ofInstant(Instant.now(), ZoneId.of(s)));
                });
    }
}
