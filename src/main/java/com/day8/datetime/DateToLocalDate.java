package com.day8.datetime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class DateToLocalDate {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("date: "+date);

        ZonedDateTime zonedDateTime = date.toInstant().atZone(ZoneId.of("America/Yakutat"));
        System.out.println("America/Yakutat: "+zonedDateTime);

        LocalDate date1 = zonedDateTime.toLocalDate();
        System.out.println(date1);

        LocalTime time = zonedDateTime.toLocalTime();
        System.out.println(time);

        ZonedDateTime zonedDateTime1 = ZonedDateTime.now();
        long l = zonedDateTime1.toEpochSecond();
        System.out.println(zonedDateTime1);
        System.out.println(new Date(l*1000));


    }
}
