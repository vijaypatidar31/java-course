package com.day1;

interface Greeting{
    void greet();
    void greet(String name);
}

public class AnonymousClass {
    public static void main(String[] args) {
        Greeting sayHello = new Greeting() {
            @Override
            public void greet() {
                System.out.println("Hello");
            }

            @Override
            public void greet(String name) {
                System.out.println("Hello! "+name);
            }
        };

        sayHello.greet();
        sayHello.greet("Ram");
    }
}
