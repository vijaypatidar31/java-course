package com.day1;

class A{
    public static int ID = 8;
    public static int FINAL_ID = 128;
    public int value;
    A(int a){
        value =a;
    }
    public void display(){
        System.out.println("id = "+ID+" value= "+value+" final_id="+FINAL_ID);
    }
}
public class StaticAndFinal {
    public static void main(String[] args) {
        A a = new A(43);
        A a1 = new A(12);

        a.display();
        a.ID = 10;
        a1.display();
    }
}
