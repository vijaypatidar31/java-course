package com.day1;

import java.util.ArrayList;

class Printer<T>{
    private T t;
    public Printer(T t){
        this.t=t;
    }
    public void print(){
        System.out.println("Printer printing "+t.toString());
    }
}
public class Generics {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("Hello");
        strings.add("hi");

        for (String a:strings)System.out.println(a);

        //custom class list
        ArrayList<User> users = new ArrayList<>();
        users.add(new User());

        User u = new User();
        u.name = "Ram";
        u.email = "ram@ayodhya.dham";
        users.add(u);

        for (User user:users){
            u.display();
        }

        // my generic class
        Printer<String> printer = new Printer<>("Vijay");
        printer.print();

        print(users);
        print(strings);
    }

    //generic method
    public static  <T> void print(ArrayList<T> ts){
        for (T t:ts){
            System.out.println(t.toString());
        }
    }
}
