package com.day1;

public class Array {
    public static void main(String[] args) {
        int[] ar = {1, 2, 3, 4, 55, 66, 1};

        for (int i = 0; i < ar.length; i++) {
            System.out.println(ar[i]);
        }

        String[] names = {"Vijay", "Ram", "Rakesh", "Rahul"};

        for (String a : names) {
            System.out.println(a);
        }

        int[][] ar2d = {
                {1},
                {1, 2},
                {1, 2, 3},
                {1, 2, 3, 4},
                {1, 2, 3, 4, 5}
        };

        for (int i = 0; i < ar2d.length; i++) {
            int[] row = ar2d[i];
            for (int j = 0; j < row.length; j++) System.out.print(row[j] + " ");
            System.out.println();
        }
    }
}
