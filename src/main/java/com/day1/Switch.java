package com.day1;

import java.util.Scanner;

public class Switch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter command(start or stop)");
        String com = scanner.nextLine();

        switch (com){
            case "start":
                System.out.println("Engine starting...");
                break;
            case "stop":
                System.out.println("Engine stop...");
                break;
            default:
                System.out.println("Invalid command");
        }
    }
}
