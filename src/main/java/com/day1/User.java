package com.day1;

public class User {
    String name;
    String email;

    public void display(){
        System.out.printf("My name is %s and my email is %s%n",name,email);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
