package com.day1;

class Person{
    String name;

    public Person(String name) {
        this.name = name;
    }

    public void display(){
        System.out.println("My name is "+name);
    }
}

class Student extends Person{
    int rollNo;

    public Student(String name, int rollNo) {
        super(name);
        this.rollNo = rollNo;
    }

    @Override
    public void display() {
        System.out.println("My name is "+name+" and roll number is "+rollNo);
    }
}
public class Casting {
    public static void main(String[] args) {
        int a = 1000;
        long l = a;//no need to define explicitly
        float v = 434.4f;
        double xz = v;
        // as storing long in int i.e large in small data type need to define explicitly
        int c = (int) l;
        int x = 10;
        short s = (short) x;
        double d = 12.4;
        float f = (float) d;

        Person person = new Person("Vijay");
        person.display();
        Student student = new Student("Ram",214);
        student.display();
        //casting without cast operator
        Person person1 = new Student("Shyam",12);
        person1.display();

        Student student1 = (Student) person1;
        student1.display();

        if (person1 instanceof Student){
            System.out.println("person1 is instance of student");
        }else{
            System.out.println("person1 is not instane of student");
        }


    }
}
