package com.day4;

import java.util.function.BiConsumer;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

public class PredicateDemo {
    public static void main(String[] args) {

        Predicate<Integer> isEven = (n)->n%2==0;

        Predicate<Integer> isPerfectSquare = (n)-> {
            int sqrt = (int)Math.sqrt(n);
            return sqrt*sqrt==n;
        };

        Predicate<Integer> isEvenAndPerfectSquare = isEven.and(isPerfectSquare);

        BiConsumer<Predicate<Integer>,Integer> caller = (integerPredicate, integer) -> System.out.println(integer+" : "+integerPredicate.test(integer));

        caller.accept(isEven,5);
        caller.accept(isEven,25);
        caller.accept(isEven,4);
        caller.accept(isEvenAndPerfectSquare,25);
        caller.accept(isEvenAndPerfectSquare,5);
        caller.accept(isEvenAndPerfectSquare,35);
        caller.accept(isEvenAndPerfectSquare,45);
        caller.accept(isEvenAndPerfectSquare,16);



    }
}
