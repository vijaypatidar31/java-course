package com.day4;

public class RunnableLambdaDemo {
    public static void main(String[] args) {
        //traditional way
        Runnable runnableT = new Runnable() {
            @Override
            public void run() {
                System.out.println("Runnable in tradition way");
            }
        };

        new Thread(runnableT).start();


        //using lambda

        Runnable runnableLambda = ()-> System.out.println("This is from runnable with lambda");

        new Thread(runnableLambda).start();

        //lambda with thread
        new Thread(()->{
           System.out.println("Lambda directly to thread");
        }).start();

        

    }
}
