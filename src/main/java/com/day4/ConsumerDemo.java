package com.day4;

import java.util.function.Consumer;

public class ConsumerDemo {
    public static void main(String[] args) {


        Consumer<Integer> sqrt = (n)-> System.out.println("Square root of "+n+" is "+Math.sqrt(n));
        Consumer<Integer> sq = (n)-> System.out.println("Square of "+n+" is "+Math.pow(n,2));

        // we can avoid code duplication within a function using this feature
        sq.accept(2);
        sqrt.accept(4);
        sq.accept(54);
        sqrt.accept(4);
        sq.accept(54);
        sqrt.accept(4);
        sq.accept(54);
    }
}
