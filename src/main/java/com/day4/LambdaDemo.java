package com.day4;

interface GreetingInterface {
    void greet();
}

@FunctionalInterface
interface IncrementInterface {
    int increment(int n, int incBy);
}

public class LambdaDemo {
    public static void main(String[] args) {

        //traditional way
        GreetingInterface sayHi = new GreetingInterface() {
            @Override
            public void greet() {
                System.out.println("Hi!...👋");
            }
        };
        sayHi.greet();

        IncrementInterface inc = new IncrementInterface() {
            @Override
            public int increment(int n, int incBy) {
                return n + incBy;
            }
        };

        System.out.println(inc.increment(5, 6));

        //with lambda expr..
        GreetingInterface sayHello = () -> System.out.println("Hello...😀 from lambda");

        sayHello.greet();

        IncrementInterface inc1 = (n, incBy) -> n + incBy;

        System.out.println(inc1.increment(20, 4));

    }
}
