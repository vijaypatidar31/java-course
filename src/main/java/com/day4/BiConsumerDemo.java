package com.day4;

import java.util.function.BiConsumer;

public class BiConsumerDemo {
    public static void main(String[] args) {

        BiConsumer<Integer,Integer> pow = (n,p)-> System.out.println(Math.pow(n,p));

        // we can avoid code duplication within a function using this feature
        pow.accept(5,3);
        pow.accept(2,13);
        pow.accept(15,4);
    }
}
