package com.day2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

class FileReadHelper {
    private final String fileName;

    public FileReadHelper(String fileName) {
        this.fileName = fileName;
    }

    public void display() {

        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fileReader);

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

            br.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found : " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

class FileWriteHelper {
    private final String fileName;

    FileWriteHelper(String fileName) {
        this.fileName = fileName;
    }

    public void create() {
        try {
            File file = new File(fileName);
            if (!file.exists())
                file.createNewFile();

            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fileWriter);

            Scanner sc = new Scanner(System.in);

            System.out.println("Enter your name");
            bw.write("\nName : " + sc.nextLine());

            System.out.println("Enter your age");
            bw.write("\nAge : " + sc.nextLine());

            System.out.println("Enter your Contact");
            bw.write("\nContact : "+sc.nextLine());

            System.out.println("Enter your email");
            bw.write("\nEmail : "+sc.nextLine());

            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.err.println("There is some erroe while creating file for you😔 : " + e.getMessage());
        }
    }
}

public class ReadingAndWritingFile {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter file name for new user record");
        String file = sc.nextLine();
        

        FileWriteHelper writeHelper = new FileWriteHelper(file);
        writeHelper.create();

        FileReadHelper readHelper = new FileReadHelper(file);
        readHelper.display();

        sc.close();
    }
}
