package com.day2;

class Task{
    boolean deadlinePast;
    public Task(boolean d){
        deadlinePast = d;
    }

    public void start() throws Exception{
        if(deadlinePast)throw new Exception("You are running out of time.😭");
        System.out.println("Task completed.😂");
    }
}

public class ExceptionDemo{
    public static void main(String...args) {
        //unchecked exception
        String[] ar = {"Book","Pen","Pencil"};

        try{
            System.err.println(ar[3]);
        }catch(Exception e){
            System.out.println("Index not exists for this array :" +e.getMessage());
        }

        //This line will also compile but cause run time exception (ArrayIndexOutOfBoundsException)
        //and undesired end of program at this point
        // System.out.println(ar[5]);

        //This line will not allow program to compile as it is throwing a checked exception
        //Thread.sleep(1000);

        //but this will work 
        try{
            Thread.sleep(1000);
        }catch(InterruptedException e){
            System.out.println(e.getMessage());
        }

        Task t1 = new Task(false);
        Task t2 = new Task(true);
        
        //this will not compile
        //t1.start();
    
        try{
            t1.start();
            t2.start();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        
    }
}