package com.day2;

enum UserType {
    ADMIN, PRINICPLE, STUDENT, FACULTY
}

public class EnumDemo {
    public static void main(String[] args) {

        UserType type = UserType.FACULTY;

        switch (type) {
            case ADMIN:
                System.out.println("User is admin");
                break;
            case FACULTY:
                System.out.println("User is faculty");
                break;
            case PRINICPLE:
                System.out.println("User is principle");
                break;
            case STUDENT:
                System.out.println("User is student");
                break;
            default:
                System.out.println("User is unkown");
                break;

        }

        // in case we get roll from database then we can use valueOf() method to get respective enum object
        UserType ut1 = UserType.valueOf("STUDENT");
        System.out.println(ut1);
    }
}
