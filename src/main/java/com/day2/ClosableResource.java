package com.day2;

import java.io.Closeable;
import java.io.IOException;

class Tap implements Closeable{

    public void open(){
        System.out.println("Tap🚰 is open, you can drink water now.😁");
    }
    @Override
    public void close(){
    
        System.out.println("Tap🚰 is closed, to save the water💧");
        
    }

}
public class ClosableResource {
    public static void main(String[] args) {
        double i = 10e5;
        System.out.println(i);
        // tap must be closed once we done with it
        Tap tap = new Tap();
        tap.open();

        tap.close();


        // after java 6
        // we don't need to call close method manually 
        try(Tap tap2 = new Tap()){
            tap2.open();
        }

    }
}
