package com.day2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Thing implements Serializable {

    private static final long serialVersionUID = -568473050300637100L;
    private String name;

    //this field not serialize
    private transient String status;


    public Thing(String name, String status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}

public class SerializationDemo {
    public static void main(String[] args) {
        writeToFile();
        readFromFile();
    }

    private static void readFromFile() {
        File file = new File("things.bin");
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream oi = new ObjectInputStream(fis);
            Object o;
            while((o=oi.readObject())!=null){
                Thing t = (Thing)o;
                System.out.println("Name: "+t.getName()+" status: "+t.getStatus());
            }
            oi.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void writeToFile() {
        File file = new File("things.bin");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(new Thing("Ball","available"));
            os.writeObject(new Thing("Pen","available"));
            os.writeObject(new Thing("Copy","available"));
            os.writeObject(null);
            os.flush();
            os.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}