package com.day2;

import java.util.ArrayList;
import java.util.List;

class Person {
    private List<String> hobbies;
    private String name;
    private String phone;

    private Person() {

    }

    public List<String> getHobbies() {
        return hobbies;
    }

    private void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    private void setPhone(String phone) {
        this.phone = phone;
    }

    public void display() {
        System.out.println("======================================");
        System.out.println("Name : " + this.name);
        System.out.println("Phone : " + this.phone);
        System.out.println("Hobbies : " + this.hobbies);
    }

    public static class Builder {
        private Person person = new Person();

        Builder() {
            person.setHobbies(new ArrayList<>());
        }

        public Person build() {
            return person;
        }

        public Builder setHobbies(List<String> hobbies) {
            person.hobbies = hobbies;
            return this;
        }

        public Builder addHobbies(String... hobbies) {
            for (String hobby : hobbies) {
                person.getHobbies().add(hobby);
            }
            return this;
        }

        public Builder setName(String name) {
            person.setName(name);
            return this;
        }

        public Builder setPhone(String phone) {
            person.setPhone(phone);
            return this;
        }

    }

}

public class InnerClass {
    public static void main(String[] args) {
        // Person person1 = new Person();
        // person1.setName("Rahul");
        // person1.setPhone("9098XXXXXXX");

        // ArrayList<String> hobbies = new ArrayList<>();
        // hobbies.add("Coding");
        // hobbies.add("Watching movie");
        // hobbies.add("Sleeping");
        // hobbies.add("Riding");

        // person1.setHobbies(hobbies);
        // person1.display();

        // using inner class

        Person person = new Person.Builder().setName("Rahul").setPhone("9098xxxxxxxx")
                .addHobbies("Coding", "Watching movie", "Sleeping", "Riding").build();
        person.display();

    }
}
