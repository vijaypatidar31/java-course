package com.day2;

class User {

    String name;
    int id;

    public User(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public void display(){
        System.out.printf("Name : %-10s ID : %d\n",name,id);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj!=null&& obj instanceof User){
            User u = (User)obj;
            return (this.id==u.id&&this.name == u.name);
        }
        return false;
    }

}

public class Equals {
    public static void main(String[] args) {
        User u1 = new User("Vijay", 1);
        u1.display();
        User u2 = new User("Ram", 2);
        u2.display();
        User u3 = new User("Vijay", 1);
        u3.display();

        System.out.println("\nWithout equals method\n");


        System.out.println("u1==u2 : "+(u1==u2));
        System.out.println("u1==u3 : "+(u1==u3));
        System.out.println("u3==u2 : "+(u3==u2));

        System.out.println("\nWith equals method\n");

        System.out.println("u1==u1 : "+(u1.equals(u2)));
        System.out.println("u1==u3 : "+(u1.equals(u3)));
        System.out.println("u3==u2 : "+(u3.equals(u2)));
    }
}
