package com.day2;

abstract class Shape{
    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract void displayDimension();
}

class Circle extends Shape{
    private int radius;
    Circle(int radius){
        this.radius = radius;
    }
    @Override
    public double getArea() {
        return (Math.PI*Math.pow(this.radius, 2));
    }

    @Override
    public double getPerimeter() {
        return Math.PI*this.radius* 2;
    }

    @Override
    public void displayDimension() {
        System.out.println("Circle : radius = "+radius+" Unit");
    }

}
class Square extends Shape{
    private int side;
    Square(int side){
        this.side = side;
    }
    @Override
    public double getArea() {
        return side*side;
    }

    @Override
    public double getPerimeter() {
        return side* 4;
    }

    @Override
    public void displayDimension() {
        System.out.println("Square : side = "+side+" Unit");
    }

}
class Rectangle extends Shape{
    private int lenght,width;
    Rectangle(int lenght,int width){
        this.lenght = lenght;
        this.width = width;
    }
    @Override
    public double getArea() {
        return width*lenght;
    }

    @Override
    public double getPerimeter() {
        return (width+lenght)*2;
    }

    @Override
    public void displayDimension() {
        System.out.println("Rectangle : length = "+lenght+" Unit , width = "+width+" unit");
    }

}


public class AbstractDemo {
    public static void main(String[] args) {
        //Can'c create object from abstract class
        //Shape shape = new Shape();

        Shape shape = new Circle(7);
        System.out.println("Area = "+shape.getArea());
        System.out.println("Circumference = "+shape.getPerimeter());
        shape.displayDimension();

        System.out.println("==================================");
        
        shape = new Square(4);
        System.out.println("Area = "+shape.getArea());
        System.out.println("Perimeter = "+shape.getPerimeter());
        shape.displayDimension();
        
        System.out.println("==================================");

        shape = new Rectangle(4,6);
        System.out.println("Area = "+shape.getArea());
        System.out.println("Perimeter = "+shape.getPerimeter());
        shape.displayDimension();

    } 
}
