package com.day2;

public class Recursion {
    static long MOD = 1000000007;
    public static long fact(long n){
        if(n<=1)return 1; 
        return n*fact(n-1)%MOD;
    }
    
    public static void main(String[] args) {
        System.out.println(fact(10));
        System.out.println(fact(120));
        System.out.println(fact(20));

        // this may cause stackoverflow exception
        // System.out.println(fact(122220));
    }
}
