package com.day5;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.LongStream;

public class TraditionalVsFunction {
    public static void main(String[] args) {

        BiConsumer<Long, Function<Long,Long>> consumer = (n, fact) -> {
            long start = System.currentTimeMillis();
            long res = fact.apply(n);
            long end = System.currentTimeMillis();
            System.out.println((end-start)+"ms taken to find fact of "+n+" which is "+res);
        };

        Function<Long,Long> factF = n ->LongStream.rangeClosed(1,n).reduce((left, right) -> left*right).getAsLong();
        Function<Long,Long> factT = n -> {
            long res =1;
            for (long i =2;i<=n;i++)res*=i;
            return res;
        };

        consumer.accept(20L,factF);
        consumer.accept(20L,factT);


    }
}
