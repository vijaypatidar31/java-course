package com.day5;


import java.util.OptionalLong;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.LongStream;
import java.util.stream.Stream;

interface UserFactory{
    User getUser(int id,String name);
}
public class FunctionDemo {
    public static void main(String[] args) {

        Function<Integer, Long> fact = integer -> {
            OptionalLong optionalLong = LongStream.rangeClosed(1, integer).reduce((left, right) -> left * right);
            return optionalLong.isPresent()?optionalLong.getAsLong():1;
        };

        Consumer<Integer> factAndPrint = (integer) -> {
            System.out.println("Fact of "+integer+" is "+fact.apply(integer));
        };

        factAndPrint.accept(1);
        factAndPrint.accept(11);
        factAndPrint.accept(5);
        factAndPrint.accept(0);

        Stream.of(1,2,4,55,6,7).forEach(factAndPrint);

        //static method reference
        BiFunction<Integer,Integer,Double> pow = Math::pow;

        System.out.println(pow.apply(2,5));

        Supplier<Integer> supplier = () -> new Random().nextInt(800)+200;

        System.out.println(supplier.get());


        //constructor reference
        UserFactory userFactory = User::new;

        System.out.println(userFactory.getUser(1,"Vijay"));

    }
}
