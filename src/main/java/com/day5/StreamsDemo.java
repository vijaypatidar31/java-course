package com.day5;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamsDemo {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Vijay", "Aman", "Shubham", "Mohit", "Rakesh", "Rahul");

        names.stream()
                .sorted()
                .filter(s -> s.matches("[^ou]*"))
                .map(String::toUpperCase)
                .forEach(System.out::println);

        System.out.println("🥴 traditional way");

        Collections.sort(names);
        for (String name : names) {
            if (name.matches("[^ou]+"))
                System.out.println(name.toUpperCase(Locale.ROOT));
        }

        //creating map
        Map<String, Integer> collect = names.stream().collect(Collectors.toMap(s -> s, String::length));
        System.out.println(collect);

        // using traditional way
        Map<String,Integer> map = new HashMap<>();
        for (String n:names){
            map.put(n,n.length());
        }
        System.out.println(map);


        //Map<String, ArrayList<Long>> key = add("key", new ArrayList<Long>());

        int asInt = IntStream.rangeClosed(0, 199).reduce((left, right) -> left + right).getAsInt();
        System.out.println("1+2+...+99+100 = "+asInt);


    }

    public static  <T,V> Map<T,V> add(T t,V v){
        Map<T,V> map = new HashMap<>();
        map.put(t,v);
        return map;
    }

}
