package com.day5;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Terminators {
    public static List<User> users = Arrays.asList(
            new User(1,"Vijay"),
            new User(2,"Rahul"),
            new User(3,"Mandeep"),
            new User(4,"Mukul"),
            new User(5,"Rahul"),
            new User(6,"Mayank"),
            new User(7,"Divya"),
            new User(8,"Sanju"),
            new User(9,"Rohit"),
            new User(10,"Rakesh"),
            new User(11,"Ram"),
            new User(12,"Vijay")
    );
    public static void main(String[] args) {



        //mapping User to name
        Function<User,String> userToName = User::getName;
        List<String> names = users.stream()
                .map(userToName)
                .collect(Collectors.toList());
        System.out.println(names);

        //Map where id is key and Object is value
        Function<User,Integer> getKey = User::getId;
        Function<User,User> getValue = (user)->user;

        Map<Integer, User> userMap = users.stream()
                .collect(Collectors.toMap(getKey, getValue));
        System.out.println(userMap);


        //Unique names and sorted
        List<String> distinctNames = users.stream()
                .map(userToName)
                .sorted()
                .distinct()
                .collect(Collectors.toList());
        System.out.println(distinctNames);

        //Unique names using set collector
        Set<String> setNames = users.stream()
                .map(userToName)
                .collect(Collectors.toSet());
        System.out.println(setNames);


        // predicates or match all,none or any one
        Predicate<User> containsIorO = user -> user.getName().matches(".*[oi].*");

        long count = users.stream().filter(containsIorO).count();
        System.out.println("count : "+count);

        boolean allUserNameContainsOorI = users.stream().allMatch(containsIorO);
        System.out.println(allUserNameContainsOorI);
        boolean atLeastOneMatchFound = users.stream().anyMatch(containsIorO);
        System.out.println(atLeastOneMatchFound);
        boolean noMatchFound = users.stream().noneMatch(containsIorO);
        System.out.println(noMatchFound);


    }
}
