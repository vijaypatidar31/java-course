package com.day7;

import java.util.Optional;
import java.util.Random;
import java.util.function.Supplier;

public class OptionalDemo {
    public static void main(String[] args) {
        Supplier<String> emailSupplier = () -> {
            if (new Random().nextBoolean()){
                return "abc@example.com";
            }else {
                return null;
            }
        };

        for (int i=0;i<10;i++){
            Optional<String> email = Optional.ofNullable(emailSupplier.get());
            if (email.isPresent()){
                System.out.println(email.get());
            }else {
                System.out.println("Email is null🥵");
            }
        }

        System.out.println("----------------");

        for (int i=0;i<10;i++){
            Optional<String> email = Optional.ofNullable(emailSupplier.get());
            //in case we only take care of non null
            email.ifPresent(System.out::println);
        }

        System.out.println("-------------------");
        //Optional of empty
        System.out.println(Optional.empty());


        System.out.println("-------------------");
        //Optional orElse

        for (int i=0;i<10;i++){
            Optional<String> email = Optional.ofNullable(emailSupplier.get());
            //in case we only take care of non null
            String mail = email.orElse("default@example.com");
            System.out.println(mail);
        }

        System.out.println("-------------------");
        //Optional orElseGet

        for (int i=0;i<10;i++){
            Optional<String> email = Optional.ofNullable(emailSupplier.get());
            //in case we only take care of non null
            String mail = email.orElseGet(emailSupplier);
            System.out.println(mail);
        }
    }


}
