package com.day7.inheritance;

public interface B {
    default void sumB(int a,int b){
        System.out.println("B.sumB : "+a+b);
    }
}
