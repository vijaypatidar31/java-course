package com.day7.inheritance;

class AB implements A,B{

}

class ABC implements A,B,C{

    //we need to override this method as it is defined in both interfaces A and C with sam sign.
    @Override
    public void sumA(int a, int b) {
        C.super.sumA(a, b);
    }
}

public class Main {
    public static void main(String[] args) {
        AB ab = new AB();
        ab.sumA(1,2);
        ab.sumB(11,2);

        ABC abc = new ABC();
        abc.sumA(12,4);
    }
}
