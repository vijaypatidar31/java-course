package com.day7.inheritance;

public interface C {
    default void sumA(int a,int b){
        System.out.println("C.sumA : "+a+b);
    }
}
