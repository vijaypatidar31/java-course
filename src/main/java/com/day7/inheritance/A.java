package com.day7.inheritance;

public interface A {
    default void sumA(int a,int b){
        System.out.println("A.sumA : "+a+b);
    }
}
