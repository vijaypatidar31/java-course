package com.day7.methods;

interface GreetInterface {
    default void sayHi(){
        System.out.println("Hi..👋");
    }

    static void greet(String name){
        System.out.println("Hello! "+name);
    }

}
class Greeter implements GreetInterface{
    @Override
    public void sayHi() {
        System.out.println("Hello....😁,to kese he aap");
    }
}
public class DefaultMethod {
    public static void main(String[] args) {
        GreetInterface greet = new GreetInterface() {};
        greet.sayHi();

        Greeter greeter = new Greeter();
        greeter.sayHi();


//        greet.greet("Vijay");//not allowed
        GreetInterface.greet("Vijay");

    }
}
