package com.day7;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PartitioningDemo {
    public static void main(String[] args) {
        List<String> names = Arrays.asList(
                "Aman",
                "Aruh",
                "Vijay",
                "Shubham",
                "Mohit",
                "Ram",
                "Ram",
                "Ravi",
                "Rohit",
                "Sanju"
        );

        Predicate<String> isNameStartingWithR = s ->s.charAt(0)=='R';

        Map<Boolean, List<String>> collect = names.stream().collect(Collectors.partitioningBy(isNameStartingWithR));
        collect.forEach((aBoolean, strings) -> {
            System.out.println("Key : "+aBoolean+" value: "+strings);
        });

        System.out.println("--------------------------");
        //to set so no duplicate in groups
        Map<Boolean, Set<String>> collect1 = names.stream().collect(Collectors.partitioningBy(isNameStartingWithR, Collectors.toSet()));
        collect1.forEach((aBoolean, strings) -> {
            System.out.println("Key : "+aBoolean+" value: "+strings);
        });


    }
}
