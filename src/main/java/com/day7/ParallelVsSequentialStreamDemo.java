package com.day7;

import java.util.function.Supplier;
import java.util.stream.IntStream;

public class ParallelVsSequentialStreamDemo {
    public static void main(String[] args) {

        //Observation: for small range parallel stream take more time then sequential


        Supplier<Long> sequential = () -> {
            long start = System.currentTimeMillis();
            for (int i = 0; i < 10; i++)
                IntStream.rangeClosed(0, Integer.MAX_VALUE/2).sequential().sum();
            return System.currentTimeMillis() - start;
        };

        Supplier<Long> parallel = () -> {
            long start = System.currentTimeMillis();
            for (int i = 0; i < 10; i++)
                IntStream.rangeClosed(0, Integer.MAX_VALUE/2).parallel().sum();
            return System.currentTimeMillis() - start;
        };

        System.out.println("Time taken by sequential : " + sequential.get() + "ms");
        System.out.println("Time taken by parallel : " + parallel.get() + "ms");

    }


}
