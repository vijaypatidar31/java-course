package com.day7.datatime;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoField;

public class LocalDateExample {
    public static void main(String[] args) {
        LocalDate date;

        date = LocalDate.now();
        System.out.println(date);

        date = LocalDate.ofYearDay(2021,32);
        System.out.println(date);

        date = LocalDate.of(2021,1,1);
        System.out.println(date);

        date = LocalDate.now();
        System.out.println(date);

        date = LocalDate.of(2020, Month.JUNE,8);
        System.out.println(date);

        System.out.println(date.getDayOfWeek().getValue());
        System.out.println(date.getDayOfWeek());

        System.out.println(date.getMonth());
        System.out.println(date.getMonthValue());

        System.out.println(date.get(ChronoField.DAY_OF_MONTH));
    }
}
