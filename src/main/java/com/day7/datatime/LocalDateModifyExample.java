package com.day7.datatime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAmount;
import java.util.Date;

public class LocalDateModifyExample {
    public static void main(String[] args) {
        LocalDate date = LocalDate.now();

        System.out.println(date);


        System.out.println(date.plusDays(5));
        System.out.println(date.plusMonths(15));
        System.out.println(date.plusYears(1));


        System.out.println(date.minusDays(5));
        System.out.println(date.minusMonths(15));
        System.out.println(date.minusYears(1));


        System.out.println(date.toEpochDay());

    }
}
