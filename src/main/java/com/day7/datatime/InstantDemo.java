package com.day7.datatime;

import java.time.Instant;

public class InstantDemo {
    public static void main(String[] args) {
        Instant instant = Instant.now();
        System.out.println("Instant of now : " + instant);

    }

    public static int gcf(int a,int b){
        System.out.println(a+" "+b);
        if (a>b)return gcf(b,a);
        if (a==0)return b;
        return gcf(b%a,a);
    }
}
