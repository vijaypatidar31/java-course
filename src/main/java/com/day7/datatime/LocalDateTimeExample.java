package com.day7.datatime;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatterBuilder;

public class LocalDateTimeExample {
    public static void main(String[] args) {
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println(dateTime);

        System.out.println("Local date : "+dateTime.toLocalDate());
        System.out.println("Local time : "+dateTime.toLocalTime());


    }
}
