package com.day7.datatime;

import java.time.LocalTime;
import java.time.temporal.ChronoField;

public class LocalTimeExample {
    public static void main(String[] args) {
        LocalTime time = LocalTime.now();
        System.out.println(time);

        time = LocalTime.of(12,3);
        System.out.println(time);

        time = LocalTime.of(12,3,1);
        System.out.println(time);

        time = LocalTime.of(12,3,1,1);
        System.out.println(time);

        System.out.println(time.get(ChronoField.AMPM_OF_DAY));

    }
}
