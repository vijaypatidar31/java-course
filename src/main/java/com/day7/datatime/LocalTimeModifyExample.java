package com.day7.datatime;

import java.time.LocalTime;
import java.time.temporal.ChronoField;

public class LocalTimeModifyExample {
    public static void main(String[] args) {
        LocalTime time = LocalTime.now();
        System.out.println(time);

        System.out.println(time.plusHours(10));
        System.out.println(time.plusMinutes(7));


    }
}
