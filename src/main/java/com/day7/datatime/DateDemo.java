package com.day7.datatime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDemo {
    public static void main(String[] args) {
        Date date = new Date();

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        System.out.println(format.format(date));

        format = new SimpleDateFormat("yyyy:MM:dd");
        System.out.println(format.format(date));

        format = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(format.format(date));


    }
}
