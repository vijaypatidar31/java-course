package com.day7.datatime;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DurationExample {
    public static void main(String[] args) {
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = start.plusDays(33);

        Duration duration = Duration.between(start,end);

        System.out.println(duration.toHours());
        System.out.println(duration.toMinutes());

        System.out.println("================");

        LocalTime timeStart = LocalTime.now();
        LocalTime timeEnd = timeStart.plusHours(5);

        duration = Duration.between(timeStart,timeEnd);
        System.out.println("Duration : "+duration.toString());
    }
}
