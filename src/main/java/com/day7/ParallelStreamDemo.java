package com.day7;

import java.util.Arrays;
import java.util.List;

public class ParallelStreamDemo {
    public static void main(String[] args) {

        List<String> names = Arrays.asList(
                "Aman",
                "Aruh",
                "Vijay",
                "Shubham",
                "Mohit",
                "Ram",
                "Ram",
                "Ravi",
                "Rohit",
                "Sanju"
        );

        names.stream().parallel().forEach(System.out::println);

        System.out.println("----------------");

        // use for and join concept of java 7
        names.stream().parallel().forEach(s->{
            System.out.println(s+"\t\t"+Thread.currentThread().getName());
        });

        System.out.println("----------------");

        // concat with parallel stream
        names.stream().parallel().reduce(String::concat).ifPresent(System.out::println);

    }


}
