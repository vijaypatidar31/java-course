package com.day6;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupByDemo {
    public static void main(String[] args) {
        List<String> names = Arrays.asList(
                "Aman",
                "Aruh",
                "Vijay",
                "Shubham",
                "Mohit",
                "Ram",
                "Ram",
                "Ravi",
                "Rohit",
                "Sanju"
        );

        //group by length of name
        Map<Integer, List<String>> namesGroupedByLength = names.stream().collect(Collectors.groupingBy(String::length));
        namesGroupedByLength.forEach((integer, strings) -> {
            System.out.println(integer + " : " + String.join(", ", strings));
        });


        System.out.println("---------------------------");
        //group by first letter
        Map<Character, List<String>> namesGroupedByFirstLetter = names.stream()
                .collect(Collectors.groupingBy(s -> s.charAt(0)));
        namesGroupedByFirstLetter.forEach((integer, strings) -> {
            System.out.println(integer + " : " + String.join(", ", strings));
        });

        System.out.println("---------------------------");
        //group by length of name
        Map<Integer, Map<String, List<String>>> collect = names.stream()
                .collect(Collectors.groupingBy(
                        String::length,
                        Collectors.groupingBy(
                                s->s.charAt(0)+""
                        )
                ));

        collect.forEach((integer, strings) -> {
            System.out.println("Length : "+integer);
            strings.forEach((s, strings1) -> {
                System.out.println(s+": "+strings1);
            });
        });

    }
}
