package com.day6;

import java.util.Random;
import java.util.stream.Stream;

public class GeneratingStream {
    public static void main(String[] args) {
        //of
        Stream<Integer> stream = Stream.of(1, 2, 22, 3, 4, 5, 6, 90);
        stream.forEach(System.out::println);

        //iterate
        Stream<Integer> evenStream = Stream.iterate(2, i -> i + 2).limit(10);
        evenStream.forEach(System.out::println);

//        Stream.iterate(1,i->i*2).limit(10).forEach(System.out::println);

        //generating stream using generate

        Stream<Integer> randomsStream = Stream.generate(() -> new Random().nextInt()).limit(10);
        randomsStream.forEach(System.out::println);


    }
}
