package com.day6;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class PrimitiveVsNormalStream {
    public static void main(String[] args) {

        Consumer<Long> consumerPrimitive = (n) -> {
            long start = System.currentTimeMillis();
            long res = LongStream.rangeClosed(1,n).reduce((left, right) -> left*right%1000000007).getAsLong();
            long end = System.currentTimeMillis();
            System.out.println((end-start)+"ms taken by primitive to find fact of "+n+" which is "+res);
        };


        Consumer<Long> consumerNonPrimitive = (n) -> {
            long start = System.currentTimeMillis();
            long res = Stream.iterate(1L,i->i+1).limit(n).reduce((left, right) -> left*right%1000000007).get();
            long end = System.currentTimeMillis();
            System.out.println((end-start)+"ms taken by non primitive to find fact of "+n+" which is "+res);
        };



        consumerNonPrimitive.accept(1L);
        consumerPrimitive.accept(1L);
        consumerNonPrimitive.accept(100L);
        consumerNonPrimitive.accept(100L);
        consumerPrimitive.accept(10L);
        consumerPrimitive.accept(40L);

    }
}
