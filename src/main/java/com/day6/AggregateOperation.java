package com.day6;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BinaryOperator;

public class AggregateOperation {
    public static void main(String[] args) {
        List<Integer> nums = Arrays.asList(1,2,44,5,66,7,8,100,-50);

        int min = nums.stream().min(Comparator.comparingInt(o -> o)).get();
        System.out.println("min: "+min);

        int max = nums.stream().max(Comparator.comparingInt(o -> o)).get();
        System.out.println("max: "+max);

        BinaryOperator<Integer> sumAg = Integer::sum;
        Integer sum = nums.stream().reduce(sumAg).get();
        System.out.println("sum: "+sum);


        System.out.println("count: "+nums.stream().count());
    }
}
