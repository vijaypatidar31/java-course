package com.day6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JoiningTerminator {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("A","BB","C","DDDD","EEEEE","FFFFF");

        String collect = list.stream().collect(Collectors.joining());
        System.out.println(collect);

        collect = list.stream().collect(Collectors.joining(","));
        System.out.println(collect);

        collect = list.stream().collect(Collectors.joining(" "));
        System.out.println(collect);

        collect = list.stream().collect(Collectors.joining(",","Start { "," }"));
        System.out.println(collect);

        //counting method is used in group by operation
        collect = list.stream().collect(Collectors.counting())+"";
        System.out.println(collect);

        //minBy
        collect = list.stream().collect(Collectors.minBy((o1, o2) -> o1.length()-o2.length())).get();
        System.out.println(collect);

        //maxBy
        collect = list.stream().collect(Collectors.maxBy((o1, o2) -> o1.length()-o2.length())).get();
        System.out.println(collect);

        //sumingint
        collect = list.stream().collect(Collectors.summingInt(String::length))+"";
        System.out.println(collect);




    }
}
