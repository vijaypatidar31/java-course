package com.day6;

import com.day5.User;

import java.util.List;
import java.util.Optional;

import static com.day5.Terminators.users;

public class FindAnyAndFirst {
    public static void main(String[] args) {
        List<User> userList = users;

        Optional<User> optionalUser = userList.stream().findAny();

        optionalUser.ifPresent(System.out::println);


        optionalUser = userList.stream().findFirst();

        optionalUser.ifPresent(System.out::println);

        //in normal stream both method will return first object if present
    }
}
