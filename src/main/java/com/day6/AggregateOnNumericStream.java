package com.day6;

import java.util.stream.IntStream;

public class AggregateOnNumericStream {
    public static void main(String[] args) {

        int sum = IntStream.rangeClosed(1,100).sum();
        System.out.println("Sum : "+sum);

        int max = IntStream.rangeClosed(1,100).max().getAsInt();
        System.out.println("Max : "+max);

        int min = IntStream.rangeClosed(1,100).min().getAsInt();
        System.out.println("Min : "+min);

        double avg = IntStream.rangeClosed(1,100).average().getAsDouble();
        System.out.println("Avg : "+avg);


    }
}
