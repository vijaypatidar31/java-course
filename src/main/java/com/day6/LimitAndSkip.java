package com.day6;

import com.day5.User;

import java.util.List;

import static com.day5.Terminators.users;

public class LimitAndSkip {
    public static void main(String[] args) {
        List<User> userList = users;

        System.out.println("All user: "+userList);

        List<User> skiped5User = userList.stream().skip(3).toList();
        System.out.println("Skip 5 user : "+skiped5User);

        List<User> limitTo3 = userList.stream().limit(3).toList();
        System.out.println("Limit to 3 : "+limitTo3);

        List<User> skipAndLimit = userList.stream().skip(4).limit(4).toList();
        System.out.println("Skip and Limit by 4 : "+skipAndLimit);
    }
}
