package com.day6;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

public class NumericStream {
    public static void main(String[] args) {
        // using of
        IntStream intStream = IntStream.of(1,2,53,5,3,5,345,3, 10);
        intStream.forEach(System.out::println);
        System.out.println("======================");

        // using iterate
        intStream = IntStream.iterate(0,i->i+2).limit(10);
        intStream.forEach(System.out::println);
        System.out.println("======================");

        // using generate
        intStream = IntStream.generate(new Random()::nextInt).limit(10);
        intStream.forEach(System.out::println);
        System.out.println("======================");
    }
}
