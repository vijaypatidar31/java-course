package com.day3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>();
        List<Integer> linked = new LinkedList<>();

        add(array);
        add(linked);

        array.clear();
        linked.clear();

        addToEnd(array);
        addToEnd(linked);

        remove(array,0);
        remove(linked, 0);

        display(array);
        display(linked);

    }

    public static void display(List<Integer> list) {
        long s = System.currentTimeMillis();
        for (int a : list) {
            // System.out.print(a + ", ");
        }
        long e = System.currentTimeMillis();
        System.out.println(
                (list instanceof ArrayList ? "ArrayList " : "LinkedList ") + (e - s) + " ms take to print list");
    }

    public static void remove(List<Integer> list, int index) {
        long s = System.currentTimeMillis();
        list.remove(index);
        long e = System.currentTimeMillis();
        System.out.println((list instanceof ArrayList ? "ArrayList " : "LinkedList ") + (e - s)
                + " ms take to remove item at " + index);
    }

    public static void add(List<Integer> list) {
        long s = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            list.add(i, 0);
        }
        long e = System.currentTimeMillis();
        System.out.println((list instanceof ArrayList ? "ArrayList " : "LinkedList ") + (e - s)
                + " ms take to add item in start of list");
    }

    public static void addToEnd(List<Integer> list) {
        long s = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            list.add(i);
        }
        long e = System.currentTimeMillis();
        System.out.println((list instanceof ArrayList ? "ArrayList " : "LinkedList ") + (e - s)
                + " ms take to add in end of list");
    }

}
