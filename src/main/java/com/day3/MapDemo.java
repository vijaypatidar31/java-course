package com.day3;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class MapDemo {
    public static void main(String[] args) {
        Map<Integer,String> map = new HashMap<>();
        
        map.put(1, "Ram");
        map.put(2, "Rahul");
        map.put(3, "Rakesh");
        map.put(4, "Vijay");

        System.out.println("key : 1 value : "+map.get(1));
        System.out.println("key : 2 value : "+map.get(2));
        System.out.println("key : 5 value : "+map.get(5));
        System.out.println("key : 5 value : "+map.getOrDefault(5,"Not found in map"));


        System.out.println("\nLoop through map");

        Set<Entry<Integer, String>> entrySet = map.entrySet();

        for(Entry<Integer, String> entry:entrySet){
            System.out.print("key : "+entry.getKey()+" value : "+entry.getValue());
        }

    }
}
