package com.day3;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
    public static void main(String[] args) {

        // Set<String> set = new HashSet<>();
        Set<String> set = new TreeSet<>();
        // Set<String> set = new LinkedHashSet<>();

        set.add("One");
        set.add("Two");
        set.add("Three");
        set.add("One");
        set.add("Five");

        for (String n : set) {
            System.out.println(n);
        }

        System.out.println("size : " + set.size());
    }
}
