package com.day3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class Animal implements Iterable<String> {

    private List<String> animals = new ArrayList<>();

    public void add(String name) {
        animals.add(name);
    }

    @Override
    public Iterator<String> iterator() {
        return animals.iterator();
    }

}

public class IteratorDemo {
    public static void main(String[] args) {
        
        Animal animal = new Animal();

        animal.add("Cow");
        animal.add("Dog");
        animal.add("Cat");
        animal.add("Rabbit");

        for(String a : animal){
            System.out.println(a);
        }


    }

}
