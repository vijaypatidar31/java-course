package com.day3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Fruit {
    private String name;
    private int price;

    public Fruit(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Name = " + name + ", price=" + price + "\n";
    }

}

class SortByName implements Comparator<Fruit> {
    private boolean reverse;

    public SortByName(boolean reverse) {
        this.reverse = reverse;
    }

    public SortByName() {

    }

    @Override
    public int compare(Fruit o1, Fruit o2) {
        if (!reverse) {
            return o2.getName().compareTo(o1.getName());
        } else {
            return o1.getName().compareTo(o2.getName());
        }
    }

}

class SortByPrice implements Comparator<Fruit> {
    private boolean reverse;

    public SortByPrice(boolean reverse) {
        this.reverse = reverse;
    }

    public SortByPrice() {

    }

    @Override
    public int compare(Fruit o1, Fruit o2) {
        if (!reverse) {
            return o2.getPrice()-o1.getPrice();
        } else {
            return o1.getPrice()-o2.getPrice();
        }
    }

}

public class Sorting {
    public static void main(String[] args) {

        List<Fruit> fruits = new ArrayList<>();

        fruits.add(new Fruit("Orange", 50));
        fruits.add(new Fruit("Apple", 150));
        fruits.add(new Fruit("Grapes", 80));
        fruits.add(new Fruit("Banana", 20));
        fruits.add(new Fruit("Water Melon", 25));

        System.out.println("=============Without any sort=============");
        System.out.println(fruits);

        System.out.println("=============Sort by name=============");
        Collections.sort(fruits, new SortByName());
        System.out.println(fruits);

        System.out.println("=============Sort by name reverse=============");
        Collections.sort(fruits, new SortByName(true));
        System.out.println(fruits);

        System.out.println("=============Sort by price=============");
        Collections.sort(fruits, new SortByPrice());
        System.out.println(fruits);

        System.out.println("=============Sort by price desc.=============");
        Collections.sort(fruits, new SortByPrice(true));
        System.out.println(fruits);

    }
}
