package com.day3;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();

        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);

        for (int a : queue) {
            System.out.println("For each : " + a);
        }

        System.out.println(queue.element());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());

        // does not throw exception even when queue is empty
        System.out.println(queue.poll());

        try {
            // throw exception even when queue is empty
            System.out.println(queue.remove());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
