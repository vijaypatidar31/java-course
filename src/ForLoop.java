
public class ForLoop {

    public static void main(String[] args) {
        int i = 2;
        int n = 23;

        int flag = 1;
        while (i < n) {
            if (n % i == 0) {
                flag = 0;
                break;
            }
            i++;
        }

        System.out.println(flag==1?"Prime":"Not prime");
    }
}