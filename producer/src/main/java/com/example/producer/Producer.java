package com.example.producer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer {
    private final ArrayBlockingQueue<Integer> arrayBlockingQueue;
    private final AtomicInteger counter = new AtomicInteger(0);
    public Producer(ArrayBlockingQueue<Integer> arrayBlockingQueue) {
        this.arrayBlockingQueue = arrayBlockingQueue;
    }

    public void startProduction(){
        Runnable runnable = () -> {
            while (true){
                try {
                    int i = counter.incrementAndGet();
                    arrayBlockingQueue.add(i);
                    System.out.println("Producer : "+i);
                    TimeUnit.MILLISECONDS.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        new Thread(runnable).start();
        new Thread(runnable).start();
    }
}
